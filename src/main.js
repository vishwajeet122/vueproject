import Vue from "vue";
import App from "./App.vue";
import route from './route';
import VueRouter from 'vue-router';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
    routes: route,
    mode: 'history',
    path: '*',
    redirect: '/'
});

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});