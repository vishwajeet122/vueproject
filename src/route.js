import Home from "./components/Home.vue";
import going from "./components/going.vue";
import VueJs from "./components/VueJs.vue";
import SQL from "./components/SQL.vue";
import Angular from "./components/Angular.vue";
import MongoDB from "./components/MongoDB.vue";


export default [
    {
        path: '',
        component: Home,
        meta: {
            auth: true,
            title: 'Vish-Home'
        }
    },{
        path: '/going',
        component: going,
        meta: {
            auth: true,
            title: 'Vish-going'
        }
    },{
        path: '/VueJs',
        component: VueJs,
        meta: {
            auth: true,
            title: 'Vish-VueJs'
        }
    },{
        path: '/SQL',
        component: SQL,
        meta: {
            auth: true,
            title: 'Vish-SQL'
        }
    },{
        path: '/Angular',
        component: Angular,
        meta: {
            auth: true,
            title: 'Vish-Angular'
        }
    },{
        path: '/MongoDB',
        component: MongoDB,
        meta: {
            auth: true,
            title: 'Vish-MongoDB'
        }
    },
];